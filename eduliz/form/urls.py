from django.urls import path
from .views import index, Qnsomos, servicios, contactanos, reservar#, registrar
from django.contrib.auth.views import LoginView, LogoutView

urlpatterns =[
    path('', index, name='index'),
    path('Qnsomos/', Qnsomos, name='Qnsomos'),
    path('servicios/', servicios, name='servicios'),
    path('contactanos/', contactanos, name='contactanos'),
   # path('registrar/', registrar, name="registrar"),
    path('reservar/', reservar, name="reservar"),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(next_page='index'), name="logout")
]

