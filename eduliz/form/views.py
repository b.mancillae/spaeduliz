from django.shortcuts import render
from django.contrib.auth.forms import UserCreationForm

# Create your views here.

def index(request):
    return render(request, 'form/index.html')

def Qnsomos(request):
    return render(request, 'form/Qnsomos.html')

def servicios(request):
    return render(request, 'form/servicios.html')

def contactanos(request):
    return render(request, 'form/contactanos.html')

#def registrar(request):
 #   return render(request, 'form/registrar.html')

def reservar(request):
   return render(request, 'form/reservar.html')   