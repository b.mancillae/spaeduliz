$(function () {

});


$.validator.addMethod("frun", function (value, element) {
    return this.optional(element) || /^0*(\d{1,3}(\.?\d{3})*)\-?([\dkK])$/.test(value);
}, "Debe ingresar un formato de run valido");

$.validator.addMethod("justtext", function (value, element) {
    return this.optional(element) || /^[a-zA-Z ]/.test(value);
}, "Debe ingresar solo texto");

$.validator.addMethod("minage", function (value, element, rules) {
    date = moment(value,"DD/MM/YYYY");
    console.log(value)
    console.log(date)
    console.log(age)
    return  this.optional(element) || age > rules;
},"");

$("#formularioReserva").validate({
    rules: {
        email: {
            required: true,
            email: true
        },
        run: {
            required: true,
            frun: true,
            max: 12
        },
        nombre: {
            required: true,
            justtext: true
        },
        fecha_reserva: {
            required: true,
            minage: 2019
        },
        hora: {
            required: true,
            min: 1
        },
        servicio: {
            required: true,
            min: 1
        },
        telefono: {
            number: true
        },
    },
    messages: {
        email: {
            required: "El correo es requerido",
            email: "Debe ingresar un correo electrónico con el formato correcto"
        },
        run: {
            required: "El RUN es requerido",
            frun: "Debe ingresar un RUN válido",
            max: ""
        },
        nombre: {
            required: "El nombre es requerido",
            justtext: "Debe ingresar solo texto"
        },
        fecha_reserva: {
            maxage:  "Año debe ser mayor a ",
            required: "La fecha de reserva es requerida",
        },
        hora: {
            required: "La hora es requerida",
            min: "Debe seleccionar una opción"
        },
        servicio: {
            required: "El servicio es requerido",
            min: "Debe seleccionar un una opción"
        },
        telefono: {
            number: "Debe ingresar solo números"
        }
    }});