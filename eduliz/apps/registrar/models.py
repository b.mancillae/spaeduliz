from django.db import models

# Create your models here.
class Registrar(models.Model):
	rut_cliente = models.CharField(max_length=10)
	nombre_cliente = models.CharField(max_length=15)
	apellido_cliente = models.CharField(max_length=15)
	telefono = models.CharField(max_length=10)
	email = models.EmailField()
	fechaNacimiento = models.DateField()
	def __str__(self):
		return '{} {}'.format(self.rut_cliente, self.nombre_cliente)
