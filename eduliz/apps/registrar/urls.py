from django.conf.urls import url
from apps.registrar.views import registrar, registrarCreate, registrarList

urlpatterns = [
    url(r'^$', registrar, name='registrar'),
    url(r'^nuevo$', registrarCreate.as_view(), name='registrar'),
    url(r'^listar$', registrarList.as_view(), name='registrar_listar'),    
]