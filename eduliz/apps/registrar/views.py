from django.shortcuts import render
from django.http import HttpResponse
from apps.registrar.form import RegistrarForm
from apps.registrar.models import Registrar
from datetime import datetime
from django.views.generic import CreateView, ListView
from django.urls import reverse_lazy

# Create your views here.

def registrar(request):
    return HttpResponse('registrar/registrar.html')

class registrarCreate (CreateView):
    model = Registrar
    form_class = RegistrarForm
    template_name = 'registrar/registrar.html'
    success_url = reverse_lazy('registrar_listar')  

class registrarList (ListView):
    model = Registrar
    template_name = 'registrar/registrar_listar.html'    