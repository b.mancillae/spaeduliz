# Generated by Django 2.2.5 on 2019-11-10 16:56

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Registrar',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('rut_cliente', models.CharField(max_length=10)),
                ('nombre_cliente', models.CharField(max_length=15)),
                ('apellido_cliente', models.CharField(max_length=15)),
                ('telefono', models.CharField(max_length=10)),
                ('email', models.EmailField(max_length=254)),
                ('fechaNacimiento', models.DateField()),
               ],
        ),
    ]
