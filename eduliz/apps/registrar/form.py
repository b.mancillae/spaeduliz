from django import forms
from apps.registrar.models import Registrar

class RegistrarForm(forms.ModelForm):

    class Meta:
        model = Registrar

        fields = [
                 'rut_cliente',
            'nombre_cliente',
            'apellido_cliente',
            'telefono',
            'email',
            'fechaNacimiento'
        ]

        labels ={
          'rut_cliente': 'Rut Cliente',
            'nombre_cliente': 'Nombre Cliente',
            'apellido_cliente': 'Apellido Cliente',
            'telefono': 'Teléfono',
            'email': 'Email',
            'fechaNacimiento': 'Fecha Nacimiento'
   
       }

        widgets = {
            'rut_cliente': forms.TextInput(attrs={'class':'form-control'}),
            'nombre_cliente': forms.TextInput(attrs={'class':'form-control'}),
            'apellido_cliente': forms.TextInput(attrs={'class':'form-control'}),
            'telefono': forms.TextInput(attrs={'class':'form-control'}),
            'email': forms.TextInput(attrs={'class':'form-control' ,'type':'email'}),
            'fechaNacimiento': forms.DateTimeInput(attrs={'class':'form-control datetimepicker-input', 'type':'date'})
      }