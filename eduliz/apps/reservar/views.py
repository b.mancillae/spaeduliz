from django.shortcuts import render, redirect
from django.http import HttpResponse
from apps.reservar.forms import ReservarForm
from apps.reservar.models import Reservar
from datetime import datetime
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

# Create your views here.

def confirmacion(request):
    return render(request,'Reservar/confirmacion.html')

def reservar_view(request):
    if request.method == 'POST':
        form = ReservarForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect(request,'reservar:confirmacion')
    else:
        form = ReservarForm()
        return render(request, 'reservar/reservar.html', {'form':form})      

def reservar_delete(request, id_reservar):
    reservar= Reservar.objects.get(id_reservar)
    if request.method == 'POST':
        reservar.delete()
        return redirect(request, 'reservar:reservar_listar')
    return render(request, 'reservar/reservar_delete.html', {'reservar':reservar})            

class reservarList (ListView):
    model = Reservar
    template_name = 'reservar/reservar_listar.html'


class reservarCreate (CreateView):
    model = Reservar
    form_class = ReservarForm
    template_name = 'reservar/reservar.html'
    success_url = reverse_lazy('reservar:reservar_listar')
    
class reservarUpdate (UpdateView):
    model = Reservar
    form_class = ReservarForm
    template_name = 'reservar/reservar.html'
    success_url = reverse_lazy('reservar:reservar_listar')
    
class reservarDelete (DeleteView):
    model = Reservar
    template_name = 'reservar/reservar.html'
    success_url = reverse_lazy('reservar:reservar_listar')