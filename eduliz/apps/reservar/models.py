from django.db import models
from datetime import datetime
from apps.cliente.models import Cliente

# Create your models here.

class Reservar(models.Model):
  fechaReserva = models.DateTimeField(default=datetime.now, null=False, blank=False)
  rut = models.ForeignKey(Cliente, null=True, blank=False, on_delete=models.CASCADE)
  NOMBRE=(
    ('Manicure','Manicure'),
     ('Pedicure','Pedicure'),
     ('Masaje','MAsaje')               
   )
  nombre = models.CharField(max_length=15, choices=NOMBRE, default='Servicio')

  def __str__(self):
     return '{} {}'.format(self.rut, self.nombre)
