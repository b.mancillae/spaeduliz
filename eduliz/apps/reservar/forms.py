from django import forms
from apps.reservar.models import Reservar


class ReservarForm(forms.ModelForm):

    class Meta: 
        model = Reservar
        
        fields = [
            'fechareserva',
            'rut',
            'nombre',
        ]

        labels ={
            'fechaReserva': 'Fecha',
            'rut': 'Cliente',
            'nombre': 'Nombre del Servicio',  
        }

        widgets = {
            'fechaReserva': forms.DateTimeInput(attrs={'class':'form-control'}),
            'rut': forms.Select(attrs={'class':'form-control'}),
            'nombre': forms.Select(attrs={'class':'form-control'}),
        }