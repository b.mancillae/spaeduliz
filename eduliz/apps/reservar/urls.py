from django.conf.urls import url, include
from apps.reservar.views import confirmacion, reservarList, reservarCreate, reservarUpdate, reservarDelete
from django.contrib.auth.decorators import login_required

urlpatterns = [
     url(r'^$', confirmacion, name='confirmacion'),
    url(r'^nuevo/$', reservarCreate.as_view(), name='reserva'),
    url(r'^listar/$', reservarList.as_view(), name='reserva_listar'),
    url(r'^editar/(?P<pk>\d+)/$', reservarUpdate.as_view(), name='reserva_editar'),
    url(r'^eliminar/(?P<pk>\d+)/$', reservarDelete.as_view(), name='reserva_eliminar'),
]