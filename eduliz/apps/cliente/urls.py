from django.conf.urls import url
from apps.cliente.views import index_cliente
urlpatterns = [
    url(r'^$', index_cliente)
]